from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("TELEPHONY")
def config_smsgw(request):
    template = loader.get_template("django_cloud_panel_smsgw/config_smsgw.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Config SMS-Gateway")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
